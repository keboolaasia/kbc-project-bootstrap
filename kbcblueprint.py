import os
import sys
import subprocess

MAIN_PY_TEMPLATE = """#!/bin/env python3
from YOURPROJECT import main
import sys
import os
import traceback

if __name__ == "__main__":
    try:
        main(datadir=os.getenv('KBC_DATADIR'))
    except (ValueError, KeyError) as err:
        print(err, file=sys.stderr)
        sys.exit(1)
    except Exception as err:
        print(err, file=sys.stderr)
        traceback.print_exc(file=sys.stderr)
        sys.exit(2)
        raise
"""

RUN_TEST_TEMPLATE = """#!/bin/sh
# flake8 --max-line-length=180 /src/
source /src/tests/env.sh
cd /src
python3 -m 'pytest' /src/tests/ "$@"
"""

MAKEFILE_TEMPLATE = """
VERSION=0.0.1
IMAGE={docker_image_name}
TESTCOMMAND="docker run --rm -it --entrypoint '/bin/ash' -v `pwd`:/src/ -e KBC_DATADIR='/src/tests/data/' ${{IMAGE}}:latest /src/run_tests.sh"
test:
	eval $(TESTCOMMAND)

run:
	docker run --rm -v `pwd`:/src/ -e KBC_DATADIR=/src/tests/data/ ${{IMAGE}}:latest

sh:
	docker run --rm -it --entrypoint "/bin/ash" -v `pwd`:/src/ -e KBC_DATADIR=/src/tests/data/ ${{IMAGE}}:latest

build:
	echo "Building ${{IMAGE}}:${{VERSION}}"
	docker build -t ${{IMAGE}}:${{VERSION}} -t ${{IMAGE}}:latest .

deploy:
	echo "Pusing to dockerhub"
	docker push ${{IMAGE}}:${{VERSION}}
	docker push ${{IMAGE}}:latest
"""

CUSTOM_PYTHON_DOCKERFILE_TEMPLATE = """FROM alpine-python3:latest

RUN mkdir -p /data/out/tables /data/in/tables /data/out/files /data/in/files
RUN pip install --no-cache-dir --ignore-installed \
    pytest \
    requests \
    && pip install --upgrade --no-cache-dir --ignore-installed https://github.com/keboola/python-docker-application/archive/2.0.0.zip

COPY . /src/

CMD python3 -u /src/main.py
"""

KBC_PYTHON_DOCKERFILE_TEMPLATE = """FROM quay.io/keboola/docker-custom-python:1.4.2

RUN mkdir -p /data/out/tables /data/in/tables /data/out/files /data/in/files

# RUN apt install your-lib

COPY . /src/
CMD python3 -u /src/main.py
"""

CUSTOM_DOCKERFILE_TEMPLATE = """FROM {image}

RUN mkdir -p /data/out/tables /data/in/tables /data/out/files /data/in/files
COPY . /src/
"""

GITIGNORE_TEMPLATE = """
# Byte-compiled / optimized / DLL files
__pycache__/
*.py[cod]
*$py.class
# Distribution / packaging
.Python
build/
develop-eggs/
dist/
downloads/
eggs/
.eggs/
lib/
lib64/
parts/
sdist/
var/
wheels/
*.egg-info/
.installed.cfg
*.egg
MANIFEST
# PyInstaller
#  Usually these files are written by a python script from a template
#  before PyInstaller builds the exe, so as to inject date/other infos into it.
*.manifest
*.spec
# Installer logs
pip-log.txt
pip-delete-this-directory.txt
# Unit test / coverage reports
htmlcov/
.tox/
.coverage
.coverage.*
.cache
nosetests.xml
coverage.xml
*.cover
.hypothesis/
# Sphinx documentation
docs/_build/
# Jupyter Notebook
.ipynb_checkpoints
**/*.ipynb
# pyenv
.python-version
# Environments
.env
.venv
env/
venv/
ENV/
env.bak/
venv.bak/
# mypy
.mypy_cache/
"""
def bootstrap_makefile(root_dir):
    print("Creating Makefile")
    docker_image_name = input("Final Docker image name (ex 'username/imagename'):\n>>> ")
    path = os.path.join(root_dir, 'Makefile')
    with open(path, 'w') as f:
        f.write(MAKEFILE_TEMPLATE.format(docker_image_name=docker_image_name))
    return path

def bootstrap_run_tests_script(root_dir):
    print("Creating run_tests.sh")
    path = os.path.join(root_dir, 'run_tests.sh')
    with open(path, 'w') as f:
        f.write(RUN_TEST_TEMPLATE)
    return path

def bootstrap_docker(root_dir, dockerfile_type):
    if dockerfile_type == 'custom-python':
        template = CUSTOM_PYTHON_DOCKERFILE_TEMPLATE
    elif dockerfile_type == 'kbc-python':
        template = KBC_PYTHON_DOCKERFILE_TEMPLATE
    elif dockerfile_type == 'custom':
        image = input("FROM: Docker image name (ex 'username/imagename:tag'):\n>>> ")
        template = CUSTOM_DOCKERFILE_TEMPLATE.format(image=image)

    path = os.path.join(root_dir, 'Dockerfile')
    with open(path, 'w') as f:
        f.write(template)
    return path

def bootstrap_git(root_dir):
    print("Creating git repo in pwd")
    subprocess.run(['git', 'init', root_dir])
    with open(os.path.join(root_dir, '.gitignore'), 'w') as f:
        f.write(GITIGNORE_TEMPLATE)

def bootstrap_python_project(root_dir):
    with open(os.path.join(root_dir, 'main.py'), 'w') as f:
        f.write(MAIN_PY_TEMPLATE)


def main():
    root_dir = '.'
    allowed_types = ['custom-python', 'custom', 'kbc-python']
    project_type = (input("Project type {}. "
                             "Default 'custom':\n>>> ".format(allowed_types))
                       or 'custom')
    if project_type not in allowed_types:
        raise ValueError(
            "Must be one of '{}', not '{}'".format(allowed_types,
                                                   project_type))
    bootstrap_git(root_dir)
    bootstrap_docker(root_dir, project_type)
    bootstrap_run_tests_script(root_dir)
    bootstrap_makefile(root_dir)
    if 'python' in project_type:
        bootstrap_python_project(root_dir)

if __name__ == '__main__':
    main()
